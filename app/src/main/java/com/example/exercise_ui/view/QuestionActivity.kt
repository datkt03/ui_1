package com.example.exercise_ui.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.exercise_ui.R
import com.example.exercise_ui.adapter.QuestionAdapter
import com.example.exercise_ui.databinding.ActivityQuestionBinding
import com.example.exercise_ui.domain.QuestionModel

class QuestionActivity : AppCompatActivity(), QuestionAdapter.score {
    private lateinit var binding: ActivityQuestionBinding
    private lateinit var questionAdapter: QuestionAdapter
    var position = 0
    var receivedList: MutableList<QuestionModel> = mutableListOf()
    var allScore = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQuestionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        receivedList = intent.getParcelableArrayListExtra<QuestionModel>("list")!!.toMutableList()

        // Update UI for the first question
        updateQuestionUI()

        binding.apply {
            imgBack.setOnClickListener {
                finish()
            }

            imgRightArrow.setOnClickListener {
                if (progressBar.progress == 10) {
                    val intent = Intent(this@QuestionActivity, ScoreActivity::class.java)
                    intent.putExtra("Score", allScore)
                    startActivity(intent)
                    finish()
                    return@setOnClickListener
                }
                position++
                updateQuestionUI()
            }

            imgLeftArrow.setOnClickListener {
                if (progressBar.progress == 1) return@setOnClickListener
                position--
                updateQuestionUI()
            }
        }
    }

    private fun updateQuestionUI() {
        binding.apply {
            progressBar.progress = position + 1
            tvQuestion.text = "Question ${progressBar.progress}/10"
            tvQuestionContent.text = receivedList[position].question

            val drawableResourceId: Int = root.resources.getIdentifier(
                receivedList[position].picPath, "drawable", root.context.packageName
            )

            Glide.with(this@QuestionActivity)
                .load(drawableResourceId)
                .centerCrop()
                .apply(RequestOptions.bitmapTransform(RoundedCorners(60)))
                .into(imgPic)

            loadAnswer()
        }
    }

    private fun loadAnswer() {
        val users: MutableList<String> = mutableListOf()
        users.add(receivedList[position].answer_1.toString())
        users.add(receivedList[position].answer_2.toString())
        users.add(receivedList[position].answer_3.toString())
        users.add(receivedList[position].answer_4.toString())

        if (receivedList[position].clickedAnswer != null) users.add(receivedList[position].clickedAnswer.toString())
        questionAdapter = QuestionAdapter(receivedList[position].correctAnswer.toString(), users, this)
        questionAdapter.differ.submitList(users)
        binding.rcvQuestion.adapter = questionAdapter
    }

    override fun amount(number: Int, clickedAnswer: String) {
        allScore += number
        receivedList[position].clickedAnswer = clickedAnswer
    }
}
