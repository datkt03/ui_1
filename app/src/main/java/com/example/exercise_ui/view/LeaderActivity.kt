package com.example.exercise_ui.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.exercise_ui.R
import com.example.exercise_ui.adapter.LeaderAdapter
import com.example.exercise_ui.databinding.ActivityLeaderBinding
import com.example.exercise_ui.domain.UserModel

class LeaderActivity : AppCompatActivity() {
    private lateinit var binding:ActivityLeaderBinding
    private lateinit var leaderAdapter: LeaderAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLeaderBinding.inflate(layoutInflater)
        setContentView(binding.root)

        leaderAdapter = LeaderAdapter()
        binding.apply {
            tvScore.text = loadData()[0].score.toString()
            tvScore2.text = loadData()[1].score.toString()
            tvScore3.text = loadData()[2].score.toString()
            tvTitleTop1.text = loadData()[0].name
            tvTitleTop2.text = loadData()[1].name
            tvTitleTop3.text = loadData()[2].name

            val drawableResourceId1:Int = binding.root.resources.getIdentifier(
                loadData()[0].pic,
                "drawable",
                binding.root.context.packageName)
            val drawableResourceId2:Int = binding.root.resources.getIdentifier(
                loadData()[1].pic,
                "drawable",
                binding.root.context.packageName)
            val drawableResourceId3:Int = binding.root.resources.getIdentifier(
                loadData()[2].pic,
                "drawable",
                binding.root.context.packageName)

            Glide.with(root.context)
                .load(drawableResourceId1)
                .into(imgShapeCrown)
            Glide.with(root.context)
                .load(drawableResourceId2)
                .into(imgShapeSecond)
            Glide.with(root.context)
                .load(drawableResourceId3)
                .into(imgShapeThird)

            bottomMenu.setItemSelected(R.id.menu_board)
            bottomMenu.setOnItemSelectedListener {
                if(it==R.id.menu_home){
                    startActivity(Intent(this@LeaderActivity,MainActivity::class.java))
                }
            }

            val list:MutableList<UserModel> = loadData()
            list.removeAt(0)
            list.removeAt(1)
            list.removeAt(2)
            leaderAdapter.differ.submitList(list)
            recycler.apply {
//                layoutManager = LinearLayoutManager(this@LeaderActivity)
                adapter = leaderAdapter
            }
        }
    }

    private fun loadData():MutableList<UserModel>{
        val users: MutableList<UserModel> = mutableListOf()
        users.add(UserModel(1,"Sophia","person1",4850))
        users.add(UserModel(2,"Daniel","person2",4750))
        users.add(UserModel(3,"James","person3",4650))
        users.add(UserModel(4,"John Smith","person4",4600))
        users.add(UserModel(5,"Emily Johnson","person5",4550))
        users.add(UserModel(6,"David Brown","person6",4450))
        users.add(UserModel(7,"Sarah Wilson","person7",4300))
        users.add(UserModel(8,"Michael Davis","person8",4250))
        users.add(UserModel(9,"Flurina de Fontaine","person9",4200))
        users.add(UserModel(10,"Anarchic","person9",4000))
        return users
    }
}