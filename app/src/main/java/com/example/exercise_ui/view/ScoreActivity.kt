package com.example.exercise_ui.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.exercise_ui.R
import com.example.exercise_ui.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {
    private lateinit var binding:ActivityScoreBinding
    private var score = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScoreBinding.inflate(layoutInflater)
        setContentView(binding.root)
        score = intent.getIntExtra("Score",0)
        binding.apply {
            tvScore.text = score.toString()
            btnBack.setOnClickListener {
                startActivity(Intent(this@ScoreActivity,MainActivity::class.java))
            }
        }
    }
}