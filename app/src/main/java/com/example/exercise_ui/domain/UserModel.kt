package com.example.exercise_ui.domain

data class UserModel(
    val id:Int,
    val name:String,
    val pic:String,
    val score:Int
)
