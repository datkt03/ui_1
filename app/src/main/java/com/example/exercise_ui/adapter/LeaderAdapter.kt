package com.example.exercise_ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.DiffResult
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.exercise_ui.databinding.ItemLeaderBinding
import com.example.exercise_ui.domain.UserModel

class LeaderAdapter: RecyclerView.Adapter<LeaderAdapter.LeaderViewHolder>() {
    private val list = mutableListOf<UserModel>()
    inner class LeaderViewHolder(private val binding:ItemLeaderBinding):
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderViewHolder {
        return LeaderViewHolder(ItemLeaderBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun getItemCount() = differ.currentList.size

    override fun onBindViewHolder(holder: LeaderViewHolder, position: Int) {
        val binding = ItemLeaderBinding.bind(holder.itemView)
        binding.apply {
            tvName.text = differ.currentList[position].name
            val drawableResourceId:Int = binding.root.resources.getIdentifier(
                differ.currentList[position].pic,
                "drawable",
                binding.root.context.packageName)
            Glide.with(binding.root.context)
                .load(drawableResourceId)
                .into(binding.imgAvatar)
            tvId.text = (position+4).toString()
            tvScore.text = differ.currentList[position].score.toString()
        }

    }
    private val differCallback = object : DiffUtil.ItemCallback<UserModel>(){
        override fun areItemsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem.id ==newItem.id
        }

        override fun areContentsTheSame(oldItem: UserModel, newItem: UserModel): Boolean {
            return oldItem == newItem
        }

    }
    val differ = AsyncListDiffer(this,differCallback)
}