package com.example.exercise_ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.exercise_ui.R
import com.example.exercise_ui.databinding.ItemQuestionBinding

class QuestionAdapter(
    private val correctAnswer: String,
    private val users: MutableList<String> = mutableListOf(),
    private var returnScore: score
) : RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder>() {

    interface score {
        fun amount(number: Int, clickedAnswer: String)
    }

    inner class QuestionViewHolder(private val binding: ItemQuestionBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        return QuestionViewHolder(
            ItemQuestionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount() = differ.currentList.size

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        val binding = ItemQuestionBinding.bind(holder.itemView)
        binding.tvAnswerTxt.text = differ.currentList[position]

        val correctPos = when (correctAnswer) {
            "a" -> 0
            "b" -> 1
            "c" -> 2
            "d" -> 3
            else -> -1
        }

        if (differ.currentList.size == 5) {
            val clickedPos = when (differ.currentList[4]) {
                "1" -> 0
                "2" -> 1
                "3" -> 2
                "4" -> 3
                else -> -1
            }

            if (position == correctPos) {
                binding.tvAnswerTxt.setBackgroundResource(R.drawable.green_bg)
                val drawable = ContextCompat.getDrawable(binding.root.context, R.drawable.tick_mdpi)
                binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, null, drawable, null
                )
            } else if (position == clickedPos && clickedPos != correctPos) {
                binding.tvAnswerTxt.setBackgroundResource(R.drawable.red_bg)
                val drawable = ContextCompat.getDrawable(binding.root.context, R.drawable.thieves_mdpi)
                binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, null, drawable, null
                )
            } else {
                binding.tvAnswerTxt.setBackgroundResource(R.drawable.answer_white)
                binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null)
            }

            holder.itemView.setOnClickListener(null)
        } else {
            binding.tvAnswerTxt.setBackgroundResource(R.drawable.answer_white)
            binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, null, null)
            holder.itemView.setOnClickListener {
                val str = when (position) {
                    0 -> "1"
                    1 -> "2"
                    2 -> "3"
                    3 -> "4"
                    else -> ""
                }
                users.add(4, str)
                notifyDataSetChanged()

                if (position == correctPos) {
                    binding.tvAnswerTxt.setBackgroundResource(R.drawable.green_bg)
                    val drawable = ContextCompat.getDrawable(binding.root.context, R.drawable.tick_mdpi)
                    binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null, drawable, null
                    )
                    returnScore.amount(5, str)
                } else {
                    binding.tvAnswerTxt.setBackgroundResource(R.drawable.red_bg)
                    val drawable = ContextCompat.getDrawable(binding.root.context, R.drawable.thieves_mdpi)
                    binding.tvAnswerTxt.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null, drawable, null
                    )
                    returnScore.amount(0, str)
                }
            }
        }
    }

    private val differCallback = object : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)
}
